# Onboarding Buddy Guide

[[_TOC_]]

## 1. Introduction

The onboarding issue is your guide for your first week. It's pretty straightforward and organized, however please keep in mind that it is written for all GitLab team members and doesn't have a lot of team specific items. There are a few subtle typos, but like everything at GitLab, it's "good enough". The "Day 1, Day 2, Day 3" structure are just guidelines, and you can complete it as fast (or as slow) as you feel comfortable with. It is okay to complete Day 1-4 in the first day or two.

With that said, you can think of this page as your unofficial Hitchhiker's Guide to GitLab that supplements the official onboarding guide and provides a Cliffnotes guided tour of the firehose of information that you will be reading in the handbook, GitLab issues, Google Docs, and code repositories.

Why isn't this in the handbook itself? Good question. I could do that, however this is a bit more personalized and opinionated and it covers a lot of ground across dozens of handbook pages since we are organized functionally, and the focus is targeted on a story that you can read cover to cover. Instead of regurgitating the content in the handbook, this is more of an list of links to the handbook pages that you should read with a few paragraphs of why it matters.

Have you ever seen a new people join your team and not work the way that the rest of the team works? This is written to help you avoid being "that person".

This also shows you how you can publish anything you want in a repository's README that may or may not be appropriate for the handbook as long as it gets written down and can be linked to. You'll read more about why that's important throughout this guide.

We'll cover other topics separately on our 1:1 calls, team calls, and Slack channels. Expect that other peers will show you around, but you will have to ask questions to prompt them to share sometimes.

## 2. Our Values

Here is your first handbook homework assignment. Please read through the following handbook pages cover-to-cover during your first week. This is at the top of your reading list in the [appendix](#appendix-reading-list).

- [Value - Collaboration](https://handbook.gitlab.com/handbook/values/#collaboration)
- [Value - Results](https://handbook.gitlab.com/handbook/values/#results)
- [Value - Efficiency](https://handbook.gitlab.com/handbook/values/#efficiency)
- [Value - Diversity, Inclusion, and Belonging](https://handbook.gitlab.com/handbook/values/#diversity-inclusion)
- [Value - Iteration](https://handbook.gitlab.com/handbook/values/#iteration)
- [Value - Transparency](https://handbook.gitlab.com/handbook/values/#transparency)

It is important that you agree with and embrace our values. Most team members join GitLab because they personally resonate with and fell in love with our values and how we work.

Please take the time to adapt your mentality and how you work to the way that the rest of GitLab works. We're here to guide and help you every step of the way. Feel free to ask anyone "I am used to doing it this way, how should I convert this to the GitLab way". It makes a great ice breaker and helps build trust.

### Not Living Our Values

Please keep in mind that brushing off the described behaviors in our values ("that's not how I work" or "assuming positive intent" and ignoring everything else) doesn't go over well here, regardless of your position or clout. If you disagree with our values, please take it up in the `#values` or `#ceo` Slack channels. Ignoring or not living our values (aka violating the values) is one of the fastest ways to lose trust with other GitLab team members and finding yourself having a discussion with a People Business Partner.

This is especially important for team members that are people managers, directors, or vice presidents where it's easy to join GitLab and want to get started on running your playbook. We've seen a large exodus of individual contributors on teams when a new management team member blatantly ignored and violated our values on a repeated basis. This guide helps you understand where we're at so you can (pre-)read the room instead of assuming that your approach will work at GitLab. We work with each other, not for each other, and people who come in and "talk at" others instead of "talk with" others will not be successful.

You will receive courtesy reminders of our values with a link of the applicable handbook section, however if you try to bring "your old way of working" to GitLab, you will quickly find yourself in culture clash situations.

- [Dont Pull Rank](https://handbook.gitlab.com/handbook/values/#dont-pull-rank)
- [What to do if values aren't being lived out](https://handbook.gitlab.com/handbook/values/#what-to-do-if-values-arent-being-lived-out)

### Best Practices

Please read the values handbook page section about [playing politics](https://handbook.gitlab.com/handbook/values/#playing-politics-is-counter-to-gitlab-values) to learn more about frowned upon behavior.

Everyone, including individual contributors, should read the [Leadership handbook page](https://handbook.gitlab.com/handbook/leadership/).

Finally, you will see a lot of references to the [Communication handbook page](https://handbook.gitlab.com/handbook/communication). It's easier to read it cover-to-cover now instead of trying to piece together section links to it later.

## 3. Handbook Pages

The handbook has a lot of content and it's a large firehose. I suggest focusing on topics that impact your lifestyle (values, benefits, communication) and your role (your department and adjacent team pages). The navigation of the handbook can be tricky at first, so I suggest starting with a department handbook page and then opening new tabs for all of the linked pages that you want to read further. You choose whether or not to bookmark all tabs to make it easier to search for URL paths later.

During your first 90 days, I suggest allocating at least 15 hours per week to reading through handbook pages and following various links to double click and discover more. It's analogous to learning things on Wikipedia and exploring a variety of linked topics. It never ends and you can never know anything, so be sure to pace yourself. Since we have flexible hours at GitLab, you can choose to read/study whenever it's best for you (ex. some people work traditional "9-5" hours, and some people sign off early in the afternoon and come back in the evenings to read/study when it's quieter at home).

The most successful people allocate 15+ hours per week. The people who struggle the most spend less than 5 hours reading what's already published and try to ask everyone questions or make more informed assumptions. It's similar to studying and doing homework in school, you have to do the homework here, but you can do it during work hours.

You should take notes of the topics that you want to discuss further on a Zoom call (referred to simply as a "call" at GitLab). You can add them as bullets on an agenda doc (always link or screenshot if able) or ask in Slack ad-hoc at any time.

**Since our handbook is public, the pro tip is to use a Google search for `gitlab handbook {keyword(s)}`.** Try it now and search for `gitlab handbook expense reports`, and read the expense policy. This is what a typical "formal" policy looks like.

Now search for `gitlab handbook gcp sandbox project` and open the `Sandbox Cloud Realm` handbook page. You can read through the entire handbook page, or look at the right sidebar to locate the `How to Get Started` section. Follow the instructions for [Individual AWS Account or GCP Project](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project) to create a GCP project, then follow the instructions for [accessing your GCP project](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#accessing-your-gcp-project). You now have a GCP project for testing whatever you want without bothering anyone and no one will step on your toes. This is an example of what self service looks like at GitLab, and what a "high bar" standard looks like if you're building processes. It doesn't need to be any more complicated than that.

Throughout this guide, and from links provided by the team, you will have a lot of handbook pages to read. Don't glaze of them, try to understand them as best as you can, so when you see things later on, you'll connect the dots on what you've already read.

At the [end of this guide](#appendix-reading-list) is a consolidated list of all of the links that are provided in this guide that is your homework reading list over your first 90 days.

## 4. Laptop Setup

The onboarding issue has instructions on initial laptop configuration. This has been iterated upon many times and may have some inconsistencies. If in doubt, just ask me or the team in Slack.

There are several access requests that occur after your first day, and if you're wondering how to get access to a system, simply ask and I'll point you in the right direction. I can also provision many of your access requests.

We expect our engineers to be self-sufficient with your hardware and tools, so I'll simply offer this list of applications that I have installed on my machine to provide you with a baseline that I suggest starting with.

If you're looking for a quick start, you can use or fork my [Dotfiles repository](https://gitlab.com/jeffersonmartin/m1-dotfiles). I can also provide additional ZSH functions with aliases for connecting to various infrastructure.

If you're not comfortable or familiar with Dotfiles, here are the applications that I recommend. Any prefixed with `[eng]` are for engineers or individual contributors and are not likely to be used by managers.

(Please install the trial for any licensed software, we'll handle MacOS software licenses by Week 2)

- [Brew](https://brew.sh/) + whatever development tools you need (ex. Python, PHP, Ruby, etc.)
- [1Password](https://1password.com/downloads/mac/) (use this for 2FA/OTP as well)
- [Google Drive](https://www.google.com/drive/download/)
- [Configure screenshots](https://www.youtube.com/watch?v=ofq91tHZvOQ) to save into a new folder in your `My Drive`.
- [Google Chrome](https://www.google.com/chrome/)
    - Do not use Google Account hot switching.
    - Create [multiple Chrome Profiles](https://support.google.com/chrome/answer/2364824?hl=en&co=GENIE.Platform%3DDesktop) (Work gitlab.com account, Personal Gmail account, Test account - local without email).
- [Slack](https://slack.com/downloads/mac)
- [Spotify](https://www.spotify.com/de-en/download/mac/) (sign in with personal account)
- [Zoom](https://zoom.us/download?os=mac)
- [NordLayer VPN](https://nordlayer.com/download/macos/) (access request required for license)
- [Logitech G Hub](https://www.logitechg.com/en-us/my-account/downloads.html) (if using a Logitech webcam)
- IDE(s) of choice - [VS Code](https://code.visualstudio.com/download), [Sublime Text](https://www.sublimetext.com/), JetBrains, etc.
- [iTerm](https://iterm2.com/downloads.html) and [Oh My ZSH](https://ohmyz.sh/)
- Git UI of choice - a few of us prefer [Fork](https://fork.dev/)
- `[eng]` [TablePlus](https://tableplus.com/download)
- `[eng]` [DBngin](https://dbngin.com/)
- `[eng]` [Laravel Herd](https://herd.laravel.com/)
- `[eng]` [Postman](https://www.postman.com/downloads/?utm_source=postman-home)

We do allow team members to use their laptop for personal use (at their discretion), including gaming or personal browsing. This includes social media, online banking and normal consumer applications. It is never a good idea to use your work computer for not-safe-for-work (NSFW) websites.

You'll be required to read the [Acceptable Use Policy](https://handbook.gitlab.com/handbook/people-group/acceptable-use-policy/) as part of the onboarding issue.

Please be aware that your computer is monitored by Jamf (MDM) and SentinelOne (EDR), including any URLs that you visit. This is only used for retroactive security investigations and we do not proactively monitor for you activity. In other words, it's not a "big brother" monitoring approach, it's for having as much forensic breadcrumb trail data as possible if your computer is breached.

- [Endpoint Management Handbook page](https://internal.gitlab.com/handbook/it/endpoint-tools/)
- [Jamf MDM Handbook Page](https://internal.gitlab.com/handbook/it/endpoint-tools/jamf/)
- [SentinelOne Handbook Page](https://internal.gitlab.com/handbook/it/endpoint-tools/sentinelone/)

You can learn more about what data we can see in this [slide deck](https://docs.google.com/presentation/d/1C2ufNXF28l0KTd5PPTkq1TjUWeWPI44VfwYbsvOzkns/edit).

## 5. Setting Expectations

Here are some unique cultural and unconventional things to know about how we work at GitLab.

### First Two Months

We give new team members breathing room for the first 4-8 weeks to get up to speed without distractions and "need your help" requests. If anyone tries to rush you, including your manager, it is safe to say something like "I'd like to take my time to onboard and learn more before I dive in, I'm happy to offer opinions in the mean time".

It's likely that you will feel that you're "shadowing" rather than "engineering" for the first 8 weeks or so, and being invited to strategy meetings at increasing frequency as time goes on.

Please plan on using this time to study as much as you can and ask questions to gain a deeper understanding of "what", "who", "how" and "why". The goal for now is to get the lay of the land and meet people and get a sense of the areas you'll be working on. Don't try to jump in and solve all the problems right away.

Take the time to do a "listening tour" with coffee chats and understand the pain points, what's mature, what's not, and understand how others think that you will be able to help them.

Don't come in with a "here's how I did it at my last job" mentality, instead try to apply your experience and lessons learned to the problems that GitLab has and look for ways to solve them based on the circumstances of the company, not just focused on your idea for the solution to the problem at hand.

### How We Communicate

Most communication is done in Slack DMs or channels (informal), GitLab issues (project/task tracking and feedback comments), or Google Docs (Zoom call meeting notes or "Agenda Docs").

### Slack

You can join and participate in any public Slack channel, and may want to start with the [list of company-wide channels](https://handbook.gitlab.com/handbook/communication/chat/) (distinct from your team's channels).

You'll get invited to the private Slack channels and we'll explain their purpose after you start.

It is helpful to ask your manager, team, or onboarding buddy for a screenshot of their Slack sidebar to see which channels they are a member of.

[Slack Handbook Page](https://handbook.gitlab.com/handbook/communication/#slack)

### Cell Phones

**Never pick up the phone and call another team member's cell phone.** It is seen as phishing/spam and will likely cause a security incident. Your cell phone number is for emergency only if you are not responding in Slack and we have an all-hands-on-deck crisis after hours and you're not a PagerDuty user. No information, confirmation, approval, forms, links, 2FA, etc are ever sent to you via SMS.

Please treat any GitLab activity related to your phone number to be suspicious and report it to `/security` on Slack. See the [phishing handbook page](https://handbook.gitlab.com/handbook/security/security-assurance/governance/phishing/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack) to learn more.

### Zoom Calls

We simply refer to these as "a Zoom" or "a call". There is no dress code. T-Shirt, T-Shirt and hoodie, polo, casual button down shirt, the choice is yours. Don't try to "dress up" if you're in a high ranking position. You'll get more respect wearing an awesome t-shirt than a button down shirt. Many people are wearing shorts or pajama pants (out of camera field of view), it's okay to dress "lazy".

It is worth ensuring you have a good quality camera and microphone and lighting in your home office.

[Zoom Handbook Page](https://handbook.gitlab.com/handbook/communication/zoom/)

### Agenda Docs

All calls other than coffee chats have a Google Doc (Agenda Doc) linked in the description of the calendar invite. You can get a taste of this by looking at all of the agenda docs for my past meetings in Google Drive (Business Technology > Engineering > Meeting Agendas). I find it insightful to look at other team member's calendars and open the agenda docs for calls for topics that interest you.

Keep in mind that all agenda docs should be shared with the company unless they are a 1:1 discussion. Others will async learn about the topics covered during your call. **Unless you are discussing money or employment issues, there is rarely a case where the document is not safe to share with the company.** "Internal" strategy is only confidential at the Director+ level, all line management level and day-to-day operations discussions are safe to share.

Ask your onboarding buddy or manager to show you how to go "calendar surfing for async information".

- [Meetings Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/meetings/)
- [Live Doc Meetings Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/live-doc-meetings/)
- [Google Docs Handbook Page](https://handbook.gitlab.com/handbook/communication/#google-docs)

### Coffee Chats

All of our 1:1 casual/social no agenda calls (watercooler, coffee, happy hour, etc) with peers or other people around the company are referred to as coffee chats. Expect to have 20+ coffee chats in your first month or two, and you should schedule at least half of them in your first few weeks as a "meet and greet". Try to meet everyone in your subdepartment/team in the first 3-4 weeks to put a face to the name, and learn more about what they think you'll be working on, and learn more about what they are working on and where synergies might exist. If your manager or onboarding buddy has not already provided you with a list of recommended people to meet, send them a message and ask to add it to your 1:1 agenda doc.

[Coffee Chat Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/informal-communication/#coffee-chats)

### Google Calendar

Your calendar should be up-to-date with any event during your normal working hours. You can set events to be private at your discretion, but we are transparent at GitLab by default for work meetings (who you're meeting with when is visible to everyone). It's totally fine to have personal events on your calendar during the work day (ex. dentist appointment, haircut, kids soccer game, gym, etc.). The Agenda Doc can have different permission levels (restricted access or available to all of GitLab) dependending on the sensitivity of the topic.

[Google Calendar Handbook Page](https://handbook.gitlab.com/handbook/communication/#google-calendar)

### Calendar Courtesies

All calls are scheduled at least 1-2 days in advance, usually the following week unless the topic is urgent. Ad-hoc calls are disruptive and discouraged unless you're engaged in a Slack threat that would be better suited by jumping on a call to hash something out quickly (ex. if you discover something at 11am, we may jump on a call at 2pm to debug together). Always look at the other person's calendar and try to schedule meetings similar to how their other meetings look (ex. clustered together, 30 min break between each meeting, on specific days of the week, etc.)

### Working Hours

I suggest starting with a 7 hour day (to allow a mental buffer) with whatever start time works best for your brain or your home life. We measure results, not hours, so it's helpful to start at a predictable hour (when to expect you'll start responding on Slack), but don't worry about "working until 5pm". Sign off when you've finished what you need to get done for the day, your brain is exhausted, or you want to spend time with your family and friends. We believe that work will always be there tomorrow, and there is no need to stress yourself out with self imposed EOD deadlines. You can see everyone's working hours in their Slack profile. Many team members are 7:00am-4:00pm, 8:00am-5:00pm or 9:00am-6:00pm in their local timezone.

### Focus Fridays

In the old days, there were no calls and no obligations on Fridays, spend the time how you see fit without interruptions. This policy was slightly modified based on the need for customer calls. If you do not interface with customers, you can still observe Focus Fridays. Anyone who tries to interrupt you should be very short and most can wait until next week. You can also add blocks on your calendar for "Focus Time" anytime during the week (ex. half of your working hours) where people will avoid scheduling calls with you during those times (or ask first).

### Time Zones

We're spread out across many time zones. The majority of the team is in the US or EMEA. The core hours for calls are approximately 7am-1pm PT, with occasional earlier calls if you're talking with someone in Europe. Our afternoons are generally quiet except for ad-hoc engineering pairs or the occasional west coast call. You do not need to be on call or respond outside of your working hours to accommodate someone else's timezone. A 24 hour later response is perfectly acceptable, up to a week or so for low priority issue comments.

### Team Calls

There are a variety of 1:1 or team calls on a weekly or bi-weekly basis that you will need to attend. We keep them to a minimum and some team members prefer to group their calls together on a specific day of the week to have focus time for the other days of the week.

Keep an eye out for invites, and open the linked agenda doc to get context of past discussions in those meetings. You can add anything you want to talk about as a numbered bullet on the agenda doc for the date of the upcoming meeting. You don't need to wait until the meeting starts, many of us will write things down 4 hours to 4 days in advance.

### Time Off

We use `Time Off by Deel` in Slack (formerly PTO Ninja and PTO by Roots) for scheduling time off. We have unlimited time off, and most team members that I talk to try for 6-8 weeks per year on average. As a rule of thumb, just put any personal appointment blocks or family obligations on the calendar, and use PTO for full days off. It's very liberal, you don't need to ask permission to take off, just try to schedule it in advance.

[Paid Time Off Handbook Page](https://handbook.gitlab.com/handbook/paid-time-off/)

### Realities of Async Work

Engineers have a much quieter calendar (2-3 calls per week), while managers have several calls each day.

Here is what you should expect for your first few months until you find your groove:

- 1-2 hours of calls each day (Monday to Thursday). Most are scheduled recurring so it's easy to plan your personal life around. Sometimes you can group most calls into 1-2 days per week, but it really varies based on your role.
- 2-3 hours per day to be spent in Slack in one form or the other. You can choose to be real-time reactive to messages throughout the day (like responding to text messages in your personal life) or come up for air at a specific time of the day and handle them all at once. It is recommended to use Slack sidebar categories to move your team members to a category so you see their messages easier. It is okay to mute most/all channels to avoid the noise of the message notifications. You will be tagged whenever you are needed in most cases. Only 10% of the channels I am a member of are unmuted and they are the ones that I provide support to any message in.
- 1-3 hours reading or responding to GitLab issues (discussions for internal business, not product problems).
- The remaining time is focus time to work on whatever project or initiative you have. The goal with onboarding is that you can use that focus time to study and get up to speed with GitLab before being responsible for delivering work.

## 6. Collaboration and Communication

### Why It Matters

Our culture encourages these important behaviors that improve our collective quality of life.

- We are spread across 65 countries and practically every timezone. This allows us to respond when its convenient for each person, so you are not being obligated to answer questions or join calls outside of your working hours since we are [friends and family first](https://handbook.gitlab.com/handbook/values/#family-and-friends-first-work-second).
- You plan in advance if you need something from someone else so not everything is treated as a fire drill or distraction, or you have patience to wait for an answer and focus on progress, not completion. People who "rush" or create a sense of urgency here are rarely successful. Take your time, do it right.
- Team members (that's you!) can stay focused on what they are working on and not constantly checking for new messages or being in pulled in different directions.
- We become focused on pursuing the vision together while being [managers of one](https://handbook.gitlab.com/handbook/leadership/#managers-of-one) for getting the work done.
- Read the [still a startup handbook page](https://handbook.gitlab.com/handbook/company/still-a-startup/) to see how we are able to stay nimble and get stuff done.

### Bias for Async Written Communication

We assume that you're already used to working remotely and at least partially asynchronously ("async"). It is important to embrace that everything moves slower at GitLab internally and we don't have the same sense of urgency that you may be be used to. A 24-72 hour response is typical and real time or 15 minute responses are not expected.

We strongly subscribe to the **this could have been an ~~email~~ Slack message or GitLab issue comment instead of a call** mindset. We have close to real time with Slack messages, however Zoom meetings are planned several days or weeks ahead instead of "as soon as possible today".

There are a lot of explanations of async communication and how to do it well. The key is to focus on **what are you trying to get an answer to, and how can I get it in the simplest way possible through written communication**. Getting a team member on a call is the last resort, not the first resort like it may be at other companies.

You need to be a very strong written communicator to succeed at GitLab, you cannot rely on jumping on calls for everything. This requires adapting your old approach of "send an email and they will reply when they can" to sending a message in Slack channel/DM or mentioning/tagging someone in a comment in a GitLab issue, and moving on to something else while waiting for their reply.

We have a large population of introvert, [neurodivergent](https://handbook.gitlab.com/handbook/values/#embracing-neurodiversity), and English-as-a-second language team members spread across 65 countries and global timezones, so async written English communication is the preferred medium whenever possible. It can feel different or even awkward at first, however you get used to it quickly and embrace working on several things at once.

If you do schedule a meeting, be sure to follow our best practices for all remote meetings. Remember that almost all meetings should be recorded and the link to the recording should be shared with the company and linked in the agenda doc.

- [Communication Handbook Page](https://handbook.gitlab.com/handbook/communication/)
- [Meetings Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/meetings/)
- [Live Doc Meetings Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/live-doc-meetings/)
- [Google Docs Handbook Page](https://handbook.gitlab.com/handbook/communication/#google-docs)
- [Effective Written Communication Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/effective-communication/)
- [Slack Handbook Page](https://handbook.gitlab.com/handbook/tools-and-tips/slack/)
- [Zoom Handbook Page](https://handbook.gitlab.com/handbook/tools-and-tips/zoom/)

### Email Communication

At GitLab, we do not use email for internal communication.

In other words, you will never email another team member or send announcement messages to ask a question or ask for approval (except for HR/People Group due to compliance requirements). You will mention/tag them in Slack thread or a GitLab issue or merge request comment.

You'll get GitLab.com notifications for issues and MRs that you're tagged in, and any email communications with vendors, but you'll never "email the team" or "send a team member a question in email".

I actually use Gmail as a search engine for all of my GitLab.com notifications of issue comments.

[Email Handbook Page](https://handbook.gitlab.com/handbook/communication/#email)

### Cell Phones

**Never try to pick up the phone and call a team member's cell phone because you have a question. It will likely be reported as a phishing attempt and create a security incident.**

We don't use cell phones for calls or text messages GitLab and cell phone plans are not expensable. The cell phone number you put in your Slack profile is for emergency use only if you're unresponsive on Slack during a crisis, and I've never been called out of the blue. It is helpful to add your (~5) immediate team members and manager to your contacts so you recognize their caller ID in an emergency, but otherwise that's the extent of "calling" or "texting" coworkers at GitLab.

You can install 1Password (for mobile browser passwords and 2FA), Slack, Gmail if you want to stay up to date on notifications while away from your desk, but you're under no obligation to use personal devices for work. In the future, you may be added to a PagerDuty schedule but I'm only paged once every 2-3 months so no worries there either.

See the [Okta Mobile and Device Trust](https://internal.gitlab.com/handbook/it/okta-device-trust/) internal handbook page to learn more about setting up your phone.

### Scheduling a Call

All "calls" at GitLab are performed using Zoom video calls. Although ad-hoc Zoom calls are allowed when agreed to during a Slack discussion of "let's jump on Zoom for a few minutes", they are discouraged to avoid disrupting the other team member who is busy with other tasks.

If a call if necessary, schedule a meeting using Google Calendar and look at their availability for something at least 24 hours away (preferrably 2-10 days way).

All call invites require a Google Doc agenda to be linked that has the topics for discussion and any pre-read links pre-filled out at least 24-48 hours in advance so everyone can come to the discussion prepared. It also allows the other team members to evaluate whether they can answer async and send you links or answers and decline the call.

[Zoom Handbook Page](https://handbook.gitlab.com/handbook/tools-and-tips/zoom/)

### Slack Communication

You should send the team member a Slack message in a DM or channel or tag/mention them in a GitLab issue for async response, or send a calendar invite at least 24 hours away (preferrably next week) with an attach Google Doc agenda with topics to discuss.

You will get a faster response in Slack (within the hour) than an issue (within a day or two), however please consider whether your question is urgent or not, and if it requires any headspace switching or background context (that they can see in an issue and related links easier than trying to discuss it in Slack).

Ask your question async and move on to something else, then come back to it. The other team member will happily reply when they are available and will schedule a Zoom call if appropriate.

[Slack Handbook Page](https://handbook.gitlab.com/handbook/tools-and-tips/slack/)

### Team Handbook Pages

Every team has a handbook page that explains their mission, what they work on, how to engage them, how they work, and usually have high-level self service answers for frequently asked questions.

Before trying to ask a "who owns" or "how do I" question in a team's Slack channel, try to find the answer on their handbook page.

This is a non-exhaustive list that gives you a jump start to start your Wikipedia-style link exploration and teaching you how to fish.

- [CEO](https://handbook.gitlab.com/handbook/ceo/)
- [Office of the CEO](https://handbook.gitlab.com/handbook/ceo/office-of-the-ceo/)
- [Finance](https://handbook.gitlab.com/handbook/finance/)
    - [Business Technology and Enterprise Applications](https://handbook.gitlab.com/handbook/business-technology/)
- [Legal](https://handbook.gitlab.com/handbook/legal/)
- [Marketing](https://handbook.gitlab.com/handbook/marketing/)
- [People](https://handbook.gitlab.com/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group)
- [Sales](https://handbook.gitlab.com/handbook/sales/#quick-reference-guide)
- [Engineering](https://handbook.gitlab.com/handbook/engineering/)
    - [DevOps Stages](https://handbook.gitlab.com/handbook/product/categories/#devops-stages)
    - [Infrastructure](https://handbook.gitlab.com/handbook/engineering/infrastructure/#organization-structure)
    - [Support](https://handbook.gitlab.com/handbook/support/)
- [Security](https://handbook.gitlab.com/handbook/security/#division-structure)

### Asking Questions

We are all busy and working on something. We don't want to distract you when you're working on something, and expect the same courtesy. Many people at GitLab get into a "groove" and know what is on their plate for the next few days and have a mental reset before starting the next week. Whenever possible, try to schedule a call for the following 1-2 weeks to avoid disrupting their flow. The other person will see the question when they have a free moment to look at Slack or issue notifications and reply when they can. That might be in 10 minutes, an hour, later today, or 3 days from now.

**This is another hard thing for new team members to get used to. You should be multitasking so that you can switch to something else instead of expecting fast responses from other team members. If you hassle people, it quickly erodes trust and gets escalated to managers that makes you look bad. It is reasonable to ping someone after 3+ days of no response, or to provide a paragraph of context of why you need this today or tomorrow.**

The fastest way to get an answer to a question is to send the user a Slack DM or tag them in a Slack channel if the question or answer might be known by others or benefit others seeing it. Always try to use the respective team's Slack channel instead of DM unless it contains confidential information. We have a lot of tribal knowledge across the company and people watch channels to not only learn from your question, but will answer it if they know the answer, resulting in crowd sourced efficiency.

Even if some topics are review or familiar, please read through these handbook pages.

- [Communication Handbook Page](https://handbook.gitlab.com/handbook/communication/)
- [All Remote Communication Guide](https://handbook.gitlab.com/handbook/company/culture/all-remote/asynchronous/)

## 7. Results and Getting Work Done

### Aligning with Our Values

It's important to know that we don't focus on perfection or polishing things internally. You'll see the term "iteration" and "work in progress (WIP)" used all the time. We focus on good enough for collaborative understanding and minimum viable changes as part of our iteration value. **This is one of the hardest things for new team members to get used to.** Unless you're in a leadership role, we don't build slide decks for "the plan", we write a few sentences/paragraph in an epic or issue, hit save, tag/mention any team members that may have input, and then get to work on getting it done. Have multiple things to work on? Create an issue for each of them, then get to work on them, or prioritize them for the backlog and switch back to working on the high priority issues.

A lot of informal collaboration and discussion happens in Slack channels. You can simply copy/paste the paragraphs or bullets that you wrote in the Slack message into the issue. Don't worry about overformatting or rigid styles, you don't get style points here for your "plan". Everything can be edited as you learn more or need to make changes. As you get familiar with the team(s) that you'll be working with, you should browse through their issue tracker projects to see how they get work.

### Writing in Markdown

Most of your written work will be in Markdown format in a GitLab issue, source code, README, handbook page, etc.

If you don't already know Markdown, see the [Handbook Markdown Guide](https://handbook.gitlab.com/docs/markdown-guide/) and [Product Markdown Guide](https://docs.gitlab.com/ee/user/markdown.html).

### Self Sufficiency

At GitLab, we are [managers of one](https://handbook.gitlab.com/handbook/leadership/#managers-of-one).

The most successful team members at GitLab are self sufficient and can get work done without being told what to do day-to-day.

You should look to your manager/director/VP/leader for the vision/direction to learn more about the high-level themes and why we are working on them. 90% of the time they are already published in the handbook or a Google Doc and you simply just need to ask for a link to where it's at if you can't find it on your own.

Once you understand the vision, most of your work (but not all) will relate to designing, iterating, problem solving, or supporting one or more components of the themes. You should collaborate with your manager or team on the "what" to do with either OKRs and KPIs.

You can figure out "how" to do it on your own, whether you already know it or figure it out through self service or collaboration with other team members. We do not treat other team members as personal assistants and ask "can you create an issue for my work?". You can self service create your own issues, send your own announcements in team channels, and get your work done without the assistance of others.

There are no bad questions, however there are unintelligent ways to ask them. Please try to do your homework before asking questions to see if it's already been answered somewhere else. The easiest way is to Google Search "gitlab handbook {keyword-or-phrase}" and look through any related/linked issues to the work that you're doing or use the search bar to find related keyword issues.

We trust that you know what to do, or can figure it out on your own. Twiddling your thumbs waiting for someone to tell you what to do, hourly or daily micro management, and "nobody taught me" is not a thing here. We expect everyone to be self starters and take initiative. If your team's workload is light, there are many other adjacent problems to solve. In most cases, there is a lot of day-to-day requests that come in, and when we have slower time we can focus on our OKR projects. If OKRs are finished ahead of schedule, there are several more in the backlog. If you're not sure of what to work on, please ask your manager for a roadmap orientation.

### Everyone Can Contribute

As a team member, you have read-write access to almost **everything** across the entire company and contribute/comment anywhere that the page loads and doesn't give you a permission error. This is part of our company mission that [everyone can contribute](https://handbook.gitlab.com/handbook/company/mission/#contribute-with-gitlab). This is another culture change that is difficult for some new team members. You do not need to ask someone for permission to interact with another team or create an issue on their issue tracker or add a comment somewhere that you have an opinion or can answer a question, just do it.

Remember that many projects are public (our transparency value), so unless an issue is marked confidential, it can be seen be everyone on the world wide web. Never let that scare you, just focus on being technically accurate and working the problem. We run our business "in public" and this can be hard to get used to. **If the issue tracker project is private, or the group above it is private, then all issues are confidentially automatically (can only be seen by team members) without marking them as confidential.**

**Do not try to "work in private" or hide anything from another team in the company. We have an "internal company only" culture, not a "team only" culture.**

### Project Work and Issue Trackers

If you're new to GitLab, there will likely be a ~~pile of work~~ several Gitlab Issues assigned to you to start sinking your teeth into. Some of it may be pre-defined and spelled out, but most likely, you were hired for a role based on a gap or pain point of "we have these (ambiguous) problems and need someone to figure out how to solve them". The assigned person is responsible for the discovery, build, design, implementation, and operationalization. It's your project, own it!

Each team has one or more [GitLab Projects](https://docs.gitlab.com/ee/user/project/organize_work_with_projects.html). A GitLab project is a large part of the value of the GitLab product, and we internally dogfood as many features as we can. You may already be familiar with how GitLab projects are used for [source code management](https://docs.gitlab.com/ee/topics/manage_code.html) and [CI/CD pipelines](https://docs.gitlab.com/ee/topics/build_your_application.html).

The majority of our work for running the business or for building the product is performed asynchronously in [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/) and [GitLab Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/). You will also want to check out [GitLab Labels](https://docs.gitlab.com/ee/user/project/labels.html).

If you've worked with GitHub Issues or JIRA before, you'll feel right at home. You will simply create or find an exisiting GitLab issue, provide details in the issue description, add or update any applied labels, and comment along the way as you work on resolving it before closing the issue when the work is done.

For your first few weeks of onboarding, just pay attention to issues and don't worry about merge requests yet.

With a few exceptions that align with our [confidentiality levels](https://handbook.gitlab.com/handbook/communication/confidentiality-levels/), you will find all company-related GitLab projects in the following top-level groups (aka "namespaces"). Each group can see the issues in all projects in the child hierarchy, so don't feel overwhelmed if you see thousands of issues, just keep navigating down to a specific issue tracker that will have dozens or a few hundred issues.

- [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com)
- [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org)

For day-to-day business at GitLab (the company), we have thousands of GitLab projects in the `gitlab-com` namespace (pronounced "gitlab dash com") that are loosely aligned with one per team, but it varies based on the needs of the team without much restriction. Most internal projects are only using the GitLab issues feature for helping the respective team plan, track, and collaborate on their work.

You can find the issue tracker for any team linked on their handbook page. Be mindful that you want to find issue trackers that have issues created or updated recently. We still have some legacy projects out there (ex. 2020, 2021, etc) that you can ignore.

Assuming that you're working in Corporate Security, the default issue tracker is [gitlab.com/gitlab-com/gl-security/corp/issue-tracker](https://gitlab.com/gitlab-com/gl-security/corp/issue-tracker/-/issues). You can see issues across all projects at the group level in [gitlab.com/gitlab-com/gl-security/corp](https://gitlab.com/groups/gitlab-com/gl-security/corp/-/issues). You can see all of the [GitLab epics](https://docs.gitlab.com/ee/user/group/epics/) in [epic list](https://gitlab.com/groups/gitlab-com/gl-security/corp/-/epics) or [gantt chart](https://gitlab.com/groups/gitlab-com/gl-security/corp/-/roadmap?state=opened&sort=START_DATE_ASC&layout=QUARTERS&timeframe_range_type=THREE_YEARS&progress=WEIGHT&show_progress=true&show_milestones=false&milestones_type=ALL&show_labels=true).

### Data Security and Confidentiality

You will get comfortable quickly with "working in public", don't sweat it or try to "hide anything". Just focus on working the problem like it's an open source project bug report. We are public/transparent by default, even with internal business operations.

Please take time to read through the [not public](https://handbook.gitlab.com/handbook/communication/confidentiality-levels/#not-public) list.

You also need to read the [data classification standard](https://handbook.gitlab.com/handbook/security/data-classification-standard/) to ensure that you don't inadvertently public YELLOW or ORANGE data in a public issue or merge request.

Things happen, and if you make a mistake, please use `/security` in Slack to get help undoing it and deleting the evidence. There is no shame here in admitting a mistake and working to get it fixed.

#### Security Team Confidentiality

Due to the nature of the risks that we mitigate, our roadmap and issue trackers are only visible to internal team members. Security incident details are on a need to know basis.

For any open source projects, all issues related to GitLab’s business, data, infrastructure, and risks are created and managed in the internal issue tracker and are linked in open source project merge requests (without disclosing title or contents).

After a risk has been mitigated and we believe that we have the latest best practices in place, we publish the documentation in our public handbook, internal handbook, and/or in the documentation.

For Security division team members, you can ask a question safely to the security team in one the following Slack channels:

- Security Department/Division Channels
    - `#security` All team members can see this and is a great place for end user questions
    - `#security-department` All team members can see this and is for department operational announcements and discussions
    - (private) `#security-team-only` Only Security team members can see this and where most internal department transparent discussions happen if they are not in an issue.
    - (private) `#security-itops` Only SIRT and IT team members for informal incident and support questions. Any super admin related questions can be posted here.
- Security Public Channels
    - `#security-corpsec`
    - `#security-corpsec-proj-*`
    - `#security-corpsec-identity`
    - `#security-corpsec-infra`
    - `#security-infrasec`
    - `#security-logging`
    - `#security-operations`
    - `#security-research`
    - `#security-risk-management`
    - `#sd_security_redteam`
    - `#sec-assurance`
    - `#sec-appsec`
    - `#sec-fieldsecurity`
    - `#sec-prod-collab`
    - `#sec-prodsec`
    - `#sec-product-security-engineering`
- Security Private Channels
    - Each team has one or more private channels for internal banter and operational discussions. Your team will invite you to those channels during your first two weeks.

### Status Reports

Each team handles status reports differently. For individual status, yuo can optionally join the `#security-team-standup` Slack channel and the Geekbot Slack application will prompt you with a "how are you doing today, what are you working on" form that you can optionally fill out. Some people like this, others don't use it. It's up to you but helps improve transparency for those who are curious. There's no need to hold back, transparency is vital here.

Most teams will post some form of Slack message on a weekly or bi-weekly basis with a bullet list of what they've been working on. Your manager or team will share how it works for your team.

## 8. Learning the GitLab Product

If you've never used GitLab (or GitHub before), you should read through the marketing page that explains the [features of the product](https://about.gitlab.com/features/). We use our own product for running our business internally; we don't have vendor tools for collaboration like Confluence, JIRA, SharePoint, etc.

For learning about the technical aspects of the GitLab product end-to-end, you can read through the documentation, you can take self-paced training classes, or you can use my Cliff Notes version that I created this awhile back that is a good syllabus for working down the list.

[Product Education and Enablement at Foundation and Intermediate level](https://about.gitlab.com/handbook/customer-success/education-enablement/). **You should plan on spending 30+ hours on the Foundations level in your first 30 days, and 50+ hours on the Intermediate level in your first 90 days.**

You can see everything related to the GitLab product development (including source code and [issues for feature requests and bugs](https://gitlab.com/gitlab-org/gitlab/-/issues)) in [gitlab.com/gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab).

To see a who's who that you can contact for specific feature questions, see the [DevOps Stages and Engineering Team Directory](https://handbook.gitlab.com/handbook/product/categories/#devops-stages) handbook page.

Before creating an issue for feature requests or bug reports, you should take a screenshot or short video and post it in the [#is-this-known](https://gitlab.slack.com/archives/CETG54GQ0) channel in Slack to get a wide number of engineers eyes on it before creating an issue. Many times you will get answers or resolution in a matter of minutes from subject matter expert(s) or [direct responsible individual (DRI)](https://handbook.gitlab.com/handbook/people-group/directly-responsible-individuals/) or [Engineering/Product team](https://handbook.gitlab.com/handbook/product/categories/#devops-stages).

## 9. Documenting What You Learned

Create a merge request on this repository to edit a section, add more context, or add an entirely new section of something that you learned or think others would benefit from knowing.

After reviewing it with your onboarding buddy, evaluate whether it should be in the [public](https://gitlab.com/gitlab-com/content-sites/handbook) or [internal](https://gitlab.com/gitlab-com/content-sites/internal-handbook) handbook, and create a merge request in the respective repository.

[Editing the Handbook](https://handbook.gitlab.com/handbook/editing-handbook/)

## Appendix. Reading List

Edit the issue description of your onboarding issue and add a section titled `Onboarding Buddy Reading List` and copy and paste these bullet points into it. You should switch the `README.md` to source code view (instead of rendered view) to copy this as native markdown.

### First Two Weeks

- [ ] Use the [Dotfiles](https://gitlab.com/jeffersonmartin/m1-dotfiles) or install and configure all of your [laptop applications](#4-laptop-setup).
- [ ] [Value - Collaboration](https://handbook.gitlab.com/handbook/values/#collaboration)
- [ ] [Value - Results](https://handbook.gitlab.com/handbook/values/#results)
- [ ] [Value - Efficiency](https://handbook.gitlab.com/handbook/values/#efficiency)
- [ ] [Value - Diversity, Inclusion, and Belonging](https://handbook.gitlab.com/handbook/values/#diversity-inclusion)
- [ ] [Value - Iteration](https://handbook.gitlab.com/handbook/values/#iteration)
- [ ] [Value - Transparency](https://handbook.gitlab.com/handbook/values/#transparency)
- [ ] [What to do if values aren't being lived out](https://handbook.gitlab.com/handbook/values/#what-to-do-if-values-arent-being-lived-out)
- [ ] [Playing politics](https://handbook.gitlab.com/handbook/values/#playing-politics-is-counter-to-gitlab-values)
- [ ] [Leadership handbook page](https://handbook.gitlab.com/handbook/leadership/)
- [ ] [Managers of one](https://handbook.gitlab.com/handbook/leadership/#managers-of-one)
- [ ] [Directly Responsible Individual (DRI)](https://handbook.gitlab.com/handbook/people-group/directly-responsible-individuals/)
- [ ] [Everyone can contribute](https://handbook.gitlab.com/handbook/company/mission/#contribute-with-gitlab)
- [ ] [Communication handbook page](https://handbook.gitlab.com/handbook/communication)
- [ ] [All Remote Communication Guide](https://handbook.gitlab.com/handbook/company/culture/all-remote/asynchronous/)
- [ ] [Email Handbook Page](https://handbook.gitlab.com/handbook/communication/#email)
- [ ] [Slack Handbook Page](https://handbook.gitlab.com/handbook/communication/#slack)
- [ ] [list of company-wide channels](https://handbook.gitlab.com/handbook/communication/chat/)
- [ ] [Zoom Handbook Page](https://handbook.gitlab.com/handbook/communication/zoom/)
- [ ] [Google Calendar Handbook Page](https://handbook.gitlab.com/handbook/communication/#google-calendar)
- [ ] [Coffee Chat Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/informal-communication/#coffee-chats)
- [ ] [Meetings Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/meetings/)
- [ ] [Live Doc Meetings Handbook Page](https://handbook.gitlab.com/handbook/company/culture/all-remote/live-doc-meetings/)
- [ ] [Google Docs Handbook Page](https://handbook.gitlab.com/handbook/communication/#google-docs)
- [ ] [Okta Mobile and Device Trust](https://internal.gitlab.com/handbook/it/okta-device-trust/)

### First 30-60 Days

#### Company Culture

- [ ] [still a startup handbook page](https://handbook.gitlab.com/handbook/company/still-a-startup/)
- [ ] [Paid Time Off Handbook Page](https://handbook.gitlab.com/handbook/paid-time-off/)
- [ ] Search Google for `gitlab handbook expense reports`
- [ ] Search Google for `gitlab handbook gcp sandbox project` and open `Sandbox Cloud Realm`. Create your GCP project.
- [ ] Read the [Acceptable Use Policy](https://handbook.gitlab.com/handbook/people-group/acceptable-use-policy/) for your work laptop.
- [ ] (Skim) [Endpoint Management Handbook page](https://internal.gitlab.com/handbook/it/endpoint-tools/)
- [ ] (Skim) [Jamf MDM Handbook Page](https://internal.gitlab.com/handbook/it/endpoint-tools/jamf/)
- [ ] (Skim) [SentinelOne Handbook Page](https://internal.gitlab.com/handbook/it/endpoint-tools/sentinelone/)
- [ ] (Skim) [SentinelOne Data Exposure Slide Deck](https://docs.google.com/presentation/d/1C2ufNXF28l0KTd5PPTkq1TjUWeWPI44VfwYbsvOzkns/edit)
- [ ] (Skim) [phishing handbook page](https://handbook.gitlab.com/handbook/security/security-assurance/governance/phishing/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack)
- [ ] [Handbook Markdown Guide](https://handbook.gitlab.com/docs/markdown-guide/)
- [ ] [Product Markdown Guide](https://docs.gitlab.com/ee/user/markdown.html)
- [ ] [Data Classification Standard](https://handbook.gitlab.com/handbook/security/data-classification-standard/)
- [ ] [Confidentiality levels](https://handbook.gitlab.com/handbook/communication/confidentiality-levels/)
- [ ] [Editing the Handbook](https://handbook.gitlab.com/handbook/editing-handbook/)

#### Product Knowledge

- [ ] [GitLab Product Features](https://about.gitlab.com/features/)
- [ ] [GitLab Projects](https://docs.gitlab.com/ee/user/project/organize_work_with_projects.html)
- [ ] [source code management](https://docs.gitlab.com/ee/topics/manage_code.html)
- [ ] [CI/CD pipelines](https://docs.gitlab.com/ee/topics/build_your_application.html)
- [ ] [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/)
- [ ] [GitLab Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/)
- [ ] [GitLab Labels](https://docs.gitlab.com/ee/user/project/labels.html)
- [ ] [GitLab Epics](https://docs.gitlab.com/ee/user/group/epics/)
- [ ] [Product Education and Enablement at Foundation Level](https://about.gitlab.com/handbook/customer-success/education-enablement/). **You should plan on spending 30+ hours on the Foundations level in your first 30 days, and 50+ hours on the Intermediate level in your first 90 days.**

#### Team Work

This list is curated for Corporate Security team members. Ask your manager, onboarding buddy, or team for a list of links specific to your team.

- [ ] [Security Vision](https://internal.gitlab.com/handbook/security/information_security_goals_and_priorities/)
- [ ] [Corporate Security Epics List](https://gitlab.com/groups/gitlab-com/gl-security/corp/-/epics)
- [ ] [Corporate Security Epics Gantt Chart](https://gitlab.com/groups/gitlab-com/gl-security/corp/-/roadmap?state=opened&sort=START_DATE_ASC&layout=QUARTERS&timeframe_range_type=THREE_YEARS&progress=WEIGHT&show_progress=true&show_milestones=false&milestones_type=ALL&show_labels=true)
- [ ] [Corporate Security Issue Tracker](https://gitlab.com/gitlab-com/gl-security/corp/issue-tracker/-/issues)
- [ ] [All Issues for CorpSec (includes other projects)](https://gitlab.com/groups/gitlab-com/gl-security/corp/-/issues)

### First 90 Days

#### Division/Organization Handbook Pages and Issue Trackers

Follow the links on pages to discover each of the teams in these divisions and what they do. Take note of any names that you want to schedule coffee chats with.

- [ ] [CEO](https://handbook.gitlab.com/handbook/ceo/)
- [ ] [Office of the CEO](https://handbook.gitlab.com/handbook/ceo/office-of-the-ceo/)
- [ ] [Finance](https://handbook.gitlab.com/handbook/finance/)
    - [ ] [Business Technology and Enterprise Applications](https://handbook.gitlab.com/handbook/business-technology/)
- [ ] [Legal](https://handbook.gitlab.com/handbook/legal/)
- [ ] [Marketing](https://handbook.gitlab.com/handbook/marketing/)
- [ ] [People](https://handbook.gitlab.com/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group)
- [ ] [Sales](https://handbook.gitlab.com/handbook/sales/#quick-reference-guide)
- [ ] [Engineering](https://handbook.gitlab.com/handbook/engineering/)
    - [ ] [DevOps Stages](https://handbook.gitlab.com/handbook/product/categories/#devops-stages)
    - [ ] [Infrastructure](https://handbook.gitlab.com/handbook/engineering/infrastructure/#organization-structure)
    - [ ] [Support](https://handbook.gitlab.com/handbook/support/)
- [ ] [Security](https://handbook.gitlab.com/handbook/security/#division-structure)

Explore some projects to learn how other teams work. You can find each team's issue tracker and projects linked from their handbook page.

- [ ] [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com)
- [ ] [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org)

#### Product Knowledge

- [ ] [Product Education and Enablement at Foundation Level](https://about.gitlab.com/handbook/customer-success/education-enablement/). **You should plan on spending 30+ hours on the Foundations level in your first 30 days, and 50+ hours on the Intermediate level in your first 90 days.**
- [ ] [GitLab Source Code](https://gitlab.com/gitlab-org/gitlab)
- [ ] [GitLab Product Issues for Features and Bugs](https://gitlab.com/gitlab-org/gitlab/-/issues)
- [ ] [DevOps Stages and Engineering Team Directory](https://handbook.gitlab.com/handbook/product/categories/#devops-stages)
